﻿using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Net;


namespace rus_word_searcher
{
  class Program
  {
    static void Main(string[] args)
    {
      const string fileName = "dictionary/russian_utf.txt";
      const string listenAddr = "127.0.0.1";
      const string listenPort = "5000";

      // if (0 == args.Length)
      // {
      //   Console.WriteLine("expected string argument");
      //   return;
      // }

      var wordsDic = new Dictionary<string, List<string>>();
      var wordsLines = System.IO.File.ReadLines(fileName);
      var searcher = new Searcher(wordsLines);

      var listener = new HttpListener();
      listener.Prefixes.Add(string.Format("http://{0}:{1}/", listenAddr, listenPort));
      
      listener.Start();

      var requestCtx = listener.GetContext();

      if ("/matches" == requestCtx.Request.Url.AbsolutePath)
      {
        var parms = requestCtx.Request.QueryString;
        var word = parms["word"];

        if (null == word)
          requestCtx.Response.StatusCode = (int)HttpStatusCode.NoContent;
        else
        {
          requestCtx.Response.StatusCode = (int)HttpStatusCode.Accepted;

          var matches = searcher.GetMatches(word);
          var matchesString = string.Join("", matches);
          var bytes = System.Text.Encoding.UTF8.GetBytes(matchesString);
          
          requestCtx.Response.OutputStream.Write(bytes);
        }
      }
      requestCtx.Response.Close();


      // foreach (var arg in args)
      // {
      //   var matches = searcher.GetMatches(arg);

      //   Console.WriteLine("found:");
      //   var builder = new System.Text.StringBuilder();
      //   builder.AppendJoin('\n', matches);

      //   Console.Write(builder.ToString());
      //   Console.Write('\n');
      // }
    }

  }
}
