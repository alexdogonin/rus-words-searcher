using System;
using System.Collections;
using System.Collections.Generic;
// using System.Linq;
using System.Threading.Tasks;

namespace rus_word_searcher {
    public class Searcher {
        public Searcher(IEnumerable<string> words)
        {
            const int defaultItemCap = 10;
            const int defaultDicCap = 50000;
            
            wordsDic = new Dictionary<string, IList<string>>(capacity:defaultDicCap);

            Parallel.ForEach(
                words,
                word =>
                {
                    var letters = word.ToCharArray();
                    Array.Sort(letters);
                    var key = String.Concat(letters);

                    IList<string> item;

                    lock (((ICollection)wordsDic).SyncRoot)
                        if (!wordsDic.TryGetValue(key, out item))
                        {
                            wordsDic.Add(key, new List<string>(capacity:defaultItemCap) { word });
                            return;
                        }

                    lock (((ICollection)item).SyncRoot)
                        item.Add(word);
                }
            );
        }
        public Searcher(IDictionary<string, IList<string>> words) => wordsDic = words;

        public IEnumerable<string> GetMatches(string word) {
            var letters = word.ToCharArray();
            Array.Sort(letters);
            var key = String.Concat(letters);

            IList<string> matches;
            return wordsDic.TryGetValue(key, out matches) ? matches : emptyList;
        }

        private IDictionary<string, IList<string>> wordsDic;

        static private readonly IEnumerable<string> emptyList = new List<string>();
    }
}