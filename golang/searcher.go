package main

import "sort"

type Searcher struct {
	dictionary map[string][]string
}

func FromLines(lines []string) *Searcher {
	dictionary := make(map[string][]string, 50000)
	for _, word := range lines {
		letters := []byte(word)
		sort.Slice(letters, func(i, j int) bool {
			return letters[i] < letters[j]
		})

		key := string(letters)
		dictionary[key] = append(dictionary[key], word)
	}

	return &Searcher{dictionary: dictionary}
}

func (s *Searcher) GetMatches(word string) []string {
	letters := []byte(word)
	sort.Slice(letters, func(i, j int) bool {
		return letters[i] < letters[j]
	})

	key := string(letters)

	return s.dictionary[key]
}
