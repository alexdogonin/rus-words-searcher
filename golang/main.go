package main

import (
	"log"
	"os"
	"strings"

	"io/ioutil"
	defaultLog "log"

	flags "github.com/jessevdk/go-flags"
)

type Options struct {
	DictionaryPath string `short:"f" long:"file-name" default:"dictionary/russian_utf.txt" description:"path to file with words"`
}

func main() {
	options := Options{}

	args := os.Args[1:]

	log.Printf("%+v", args)

	words := []string{}
	var err error
	if words, err = flags.Parse(&options); nil != err {
		if helpErr := err.(*flags.Error); nil == helpErr || flags.ErrHelp != err.(*flags.Error).Type {
			defaultLog.Printf("error parse flags, %v", helpErr)
		}
		os.Exit(1)
	}

	bytes, err := ioutil.ReadFile(options.DictionaryPath)
	if nil != err {
		defaultLog.Fatalf("read dictionary error, %v", err)
	}

	wordsLines := strings.Split(string(bytes), "\n")
	searcher := FromLines(wordsLines)

	log.Printf("found:\n")
	for _, word := range words {
		matches := searcher.GetMatches(word)

		log.Printf("%v\n", strings.Join(matches, "\n"))
	}
}
